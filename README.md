Copyright &copy; 2020 Kirk Rader

# Node-RED Template

_See [LICENSE.txt](LICENSE.txt)._

Template for creating a [Node-RED][]-based home-automation server.

See <https://kirkrader.gitlab.io/docs/rpi-config/> for information on configuring a [Raspberry Pi][pi] to run [Node-RED][] and [Mosquitto][] with secure, remote access.

```mermaid
graph TD

  subgraph Legend

    template["Component<br/>Used<br/>by Template"]
    virtual["Required<br/>Component<br/>Not Defined<br/>by Template"]
    component1["Another<br/>Component<br/>Used<br/>by Template"]
    infrastructure["Component<br/>Indirectly<br/>Assumed<br/>by Template"]

  end

  template ---|"Interface<br/>Used<br/>by Template"| component1
  template .-|"Interface<br/>Not Defined<br/>by Template"| virtual

  subgraph Architecture

    subgraph "The 'Cloud'"
      location["Mobile OS<br/>location<br/>services"]
      duckdns["Dynamic DNS service<br/>(e.g. Duck DNS)"]
      ca["Certificate Authority<br/>(e.g. Let's Encrypt)"]
    end

    subgraph Mobile Device

      browser1["Web Browser<br/>(Dashboard)"]
      owntracks["Owntracks<br/>App"]

    end

    subgraph Laptop / Desktop
      browser2["Web Browser<br/>(Dashboard, Editor)"]
    end

    subgraph Home Network

      router["Router<br/>(port<br/>forwarding)"]

      subgraph Home Automation Server
        nodered["Node-RED"]
        mosquitto["MQTT Broker<br/>(e.g. Mosquitto)"]
        cron["cron jobs<br/>(update IP address,<br/>renew certificates)"]
      end

      device1["Device 1"]
      device2["Device 2"]
      device3["Device 3"]

    end

  end

  duckdns --- cron
  ca --- cron
  location --- owntracks
  browser1 ---|HTTPS| router
  owntracks ---|MQTTS| router
  router --- |HTTPS| nodered
  router --- |MQTTS| mosquitto
  mosquitto ---|MQTTS| nodered
  nodered .- device1
  nodered .- device2
  nodered .- device3
  browser2 --- |HTTPS| router

  classDef virtual fill:pink,color:black
  class virtual,device1,device2,device3 virtual

  classDef template fill:aquamarine,color:black
  class template,component1,browser1,browser2,owntracks,nodered,mosquitto template

  classDef legend fill:white,color:black
  class Architecture,Legend legend
```

## Features

| Feature                                     | Description                    |
|---------------------------------------------|--------------------------------|
| [Lighting Automation](#lighting-automation) | Time and date based lighting themes |
| [Presence Detection](#presence-detection)   | [Owntracks][] based home / away automation |
| [Unified Dashboard](#unified-dashboard)     | Web-based, mobile-friendly user interface for all the "smart" gear in your home from multiple manufacturers |

### Lighting Automation

Lighting automation, driven by [node-red-contrib-suncalc][], uses a number of sub-flows for modularity. It also depends on several global context variables. Each time the value of `global.time-of-day` changes, a particular combination of sub-flows is invoked based on the current values of `global.time-of-day` and `global.theme`.

#### Time of Day

The output of [node-red-contrib-suncalc][] is used to set `global.time-of-day`:

| `global.time-of-day` | Description                                      |
|----------------------|--------------------------------------------------|
| `day`                | Sunrise to sunset                                |
| `evening`            | Sunset to hour indicated by `global.nighttime`   |
| `night`              | Hour indicated by `global.nighttime` to sunrise  |

Specifically, the first output of a [node-red-contrib-suncalc][] is used to evaluate the following logic once per minute:

```mermaid
graph TD

  isday{{sun is up?}}
  isevening{{hour < global.nighttime?}}
  setday[set global.time-of-day to day]
  setevening[set global.time-of-day to evening]
  setnight[set global.time-of-day to night]

  isday --> |yes| setday
  isday --> |no| isevening
  isevening --> |yes| setevening
  isevening --> |no| setnight
```

- The value of `global.nighttime` can be set using a drop-down control on the _Automation_ dashboard page

- Various sub-flows are invoked each time `global.time-of-day` changes as described below

- For simplicity of exposition, not shown in the preceding flow chart is the use of `rbe` nodes to ensure that `global.time-of-day` is actually only updated and automation triggered when the state changes

- This is similar to using the second output from [node-red-contrib-suncalc][], except the latter only supports a distinction between "day" and "night;" the once-per-minute output and the preceding logic is used so as to support the distinction between "evening" and "night"

#### Theme

The Linux `date` command is invoked every day at midnight in order to set `global.theme`:

| `global.theme` | Description                    |
|----------------|--------------------------------|
| `tribal`       | July 1-4 (US Independence Day) |
| `spooky`       | Any day in October             |
| `jolly`        | Any day in December            |
| `standard`     | Any other day                  |

Specifically, an `exec` node is used to invoke `date '+%-m %-d'` each midnight (00:00 a,k.a. 12AM) and its output is used to evaluate the following logic:

```mermaid
graph TD

  istribal{{month = 7 and day < 4?}}
  isspooky{{month = 10?}}
  isjolly{{month = 12?}}
  settribal[set global.theme to tribal]
  setspooky[set global.theme to spooky]
  setjolly[set global.theme to jolly]
  setstandard[set global.theme to standard]

  istribal --> |yes| settribal
  istribal --> |no| isspooky
  isspooky --> |yes| setspooky
  isspooky --> |no| isjolly
  isjolly --> |yes| setjolly
  isjolly --> |no| setstandard
```

- As for `global.time-of-day`, `rbe` nodes are used to update `global.theme` only at start up or when the theme changes

#### Lighting Sub-Flows

The logic that responds to changes in the value of `global.time-of-day`, described below, uses the following sub-flows:

| Sub-Flow           | Description                                             |
|--------------------|---------------------------------------------------------|
| `Interior Evening` | Activate date-based indoor lighting `global.theme`      |
| `Interior Night`   | Activate indoor nightlight settings                     |
| `Interior Off`     | Turn off indoor lighting                                |
| `Exterior Evening` | Activate date-based outdoor lighting for `global.theme` |
| `Interior Off`     | Turn off outdoor lighting                               |

- The behavior of the `Interior Evening` and `Exterior Evening` sub-flows is affected by the current value of `global.theme` as described above; i.e. they invoke specific combinations of [Hue][] "scenes" based on the time of year

- The `Interior Evening`, `Interior Night` and `Interior Off` sub-flows depend on a "zone" named `Interior` defined in the native [Hue][] app so as to affect lights only in public spaces while leaving private spaces like bedrooms alone

- The `Interior Night` sub-flow sets the `Interior` zone to the standard `Nightlight` scene built into the [Hue][] app

#### Lighting State Machine

Putting all of the preceding together, here is the overall state-machine that drives lighting automation:

```mermaid
stateDiagram-v2

  day --> evening: sunset /<br/>Interior Evening,<br/>Exterior Evening
  evening --> night: global.nighttime /<br/>Interior Night,<br/>Exterior Off
  night --> day: sunrise /<br/>Interior Off,<br/>Exterior Off
```

- Again, the behavior of the "evening" sub-flows is affected by the current value of `global.theme` as described above

### Presence Detection

Presence detection uses [MQTT][] messages sent by the [Owntracks][] app on each occupant's mobile device. This is driven by the values of `global.presence` and `global.mode` as determined by the following logic each time a [MQTT][] messages arrives matching the `owntracks/+/+` topic pattern. This logic

```mermaid
graph TD

  updatepresence["update global.presence[topic] to true or false based on payload"]
  allpresent{{all global.presence entries true?}}
  sethome[set global.mode to home]
  setaway[set global.mode to away]

  updatepresence --> allpresent
  allpresent --> |yes| sethome
  allpresent --> |no| setaway
```

- The preceding logic will automatically add new entries to `global.presence` when they are first seen as [MQTT][] topics

- Use the _Clear Presence_ button on the _Automation_ dashboard to reset `global.presence` if you want to remove occupants; this will remove all occupants' entries and then those that are currently active will be re-added automatically on receipt of subsequent messages from [Owntracks][]

- When updating the `global.presence` entry for a given topic, the logic assumes that each occupant's [Owntracks][] app is configured correctly as described [below](#home-region)

When the value `global.mode` changes, the following automation is triggered:

<table>

<tr>

<td>

| Mode   | Description                                                         |
|--------|---------------------------------------------------------------------|
| `home` | Activate [Interior Evening](#lighting-sub-flows) theme when the first person arrives |
| `away` | Invoke the [lighting automation](#lighting-automation) as if [global.time-of-day](#time-of-day) just changed |

</td>

<td>

```mermaid
stateDiagram-v2

  home --> away: last occupant leaves /<br/>resume automation
  away --> home: first occupant arrives /<br/>interior evening
```

</td>

</tr>

</table>

- **Rationale:** The `Interior Evening` sub-flow is invoked when the first occupant arrives so that the home will be lit no matter how dark or gloomy the day or night; the normal automation for the current time of day is resumed when the last person leaves as a simple form of presence simulation for security purposes

#### Home Region

As per the design of the [Owntracks][] API, when evaluating the payload of `owntracks/+/+` messages the preceding logic depend on each occupant's device to be properly configured.

1. [Owntracks][] reports the location of particular devices, not individual users in cases where a given user has more than one device with [Owntracks][] installed

2. Location reporting depends on the "regions" defined in each device's [Owntracks][] configuration

In particular, these flows assume that each combination of "tracker id" (the value corresponding to the first `+` in `owntracks/+/+`) and "device id" (the value of the second `+`) is an individual user to be tracked. If any user has a more than one device that is sending [Owntracks][] messages to the same broker the `home` / `away` state could become confused.

In addition, these flows assume that all occupants' [Owntracks][] configuration includes a region named "Home" centered at the location of the home automation server, i.e. the center of the geofence that should be used to triger `home` / `away` automation. The `update global.presence[topic]...` operation in the presence detection [flow-chart](#presence-detection)sets `global.presence[topic]` to `true` or `false` based on whether or not the `payload.inregions` array sent by [Owntracks][] includes the string `"Home"`. This allows each user to tweak [Owntracks][] settings like "reporting mode" to find a decent balance between reporting frequency and batter life to achieve acceptable presence detection accuracy. An alternative would be to require that all occupants have the highest frequency reporting mode enabled on their mobile devices and use something like [node-red-node-geofence](https://flows.nodered.org/node/node-red-node-geofence) to let [Node-RED][] drive the determination of what it means to be "home."

### Unified Dashboard

Some brands of "smart home" products come with their own proprietary apps. Such apps typically provide remote monitoring and control of only that particular brand's gear. Conversely, some makes and models of devices are designed to rely on third-party controllers using industry-standard protocols like [Z-Wave][] and [Zigbee][]. In any case, if your home automation set-up incorporates devices from more than one manufacturer you need some system like [Node-RED][] that can provide automation and a remote control user interface that works with products from diverse manufacturers.

These flows use [node-red-dashboard][] to provide a web-based, mobile-friendly user interface for monitoring and controlling the devices in in a home network. They assume that the dashboard is exposed to the Internet in a secure fashion as described below.

## Dependencies

Before loading this project, install the following into [Node-RED][] using its "manage palette" menu:

- [node-red-node-rbe][]
- [node-red-dashboard][]
- [node-red-contrib-suncalc][]
- [node-red-contrib-moment][]

You must run a local [MQTT][] broker like [Mosquitto][] or else set up an account with a "cloud" based [MQTT][] service.

Each occupant's mobile device must have [Owntracks][] installed and configured to send messages to your [MQTT][] broker. [Owntracks][] must also be configured on each device to have a "region" named _Home_ centered at the home automation server's location.

See <https://kirkrader.gitlab.io/docs/rpi-config> for information setting up a [Raspberry Pi][pi] to meet all of the preceding requirements.

## Usage

1. Enable projects in _settings.js_
    ```javascript
    editorTheme: {
        projects: {
            // To enable the Projects feature, set this value to true
            enabled: true
        }
    }
    ```
2. Enable JSON pretty-printing in _settings.js_:
   ```javascript
   flowFilePretty: true
   ```
3. Restart service:
   ```bash
   sudo systemctl restart nodered.service
   ```
4. Consider additional configuration options in _settings.js_:
    ```javascript
    // use `node-red admin hash-pw` to encrypt passwords

    // require a user name and password to access the editor
    adminAuth: {
        type: "credentials",
        users: [{
            username: "admin",
            password: "$2a$08$zZWtXTja0fB1pzD4sHCMyOCMYz2Z6dNbM6tl8sJogENOMcxWV9DN.",
            permissions: "*"
        }]
    },

    // require a user name and password to access the dashboard, HTTP end-points, etc.
    httpNodeAuth: {
      user:"user",
      pass:"$2a$08$zZWtXTja0fB1pzD4sHCMyOCMYz2Z6dNbM6tl8sJogENOMcxWV9DN."
    },

    // require encryption for the editor, dashboard and HTTP end-points
    requireHttps: true,
    ```
5. Configure your home network infrastructure
    - Set up port forwarding rules in your router for ports 1880 ([Node-RED][]) and 1883 ([MQTT][])
    - Set up dynamic DNS service (e.g. using [Duck DNS][])
    - Create security certificates (e.g. using [Let's Encrypt][])
6. Set up a [MQTT][] broker such as [Mosquitto][] or create an account with a cloud-based [MQTT][] service
    - Configure the [MQTT][] broker to require encryption and authentication
7. Install and configure [Owntracks][] on each occupant's mobile device
    - Connect to the [MQTT][] broker set up in step 6
    - Define a region named `Home` centered at the automation server's location
8. Create a new project by cloning a fork of <https://gitlab.com/parasaurolophus/node-red-project-template.git>

[pi]: https://raspberrypi.org
[hue]: https://philips-hue.com
[Z-Wave]: https://z-wavealliance.org
[MQTT]: https://mqtt.org
[HTTP]: https://www.w3.org/Protocols/
[TLS]: https://tools.ietf.org/html/rfc8446
[Duck DNS]: https://duckdns.org
[Owntracks]: https://owntracks.org
[Let's Encrypt]: https://letsencrypt.org
[Mosquitto]: https://mosquitto.org/
[Node-RED]: https://nodered.org
[node-red-node-rbe]: https://flows.nodered.org/node/node-red-node-rbe
[node-red-dashboard]: https://flows.nodered.org/node/node-red-dashboard
[node-red-contrib-suncalc]: https://flows.nodered.org/node/node-red-contrib-suncalc
[node-red-contrib-moment]: https://flows.nodered.org/node/node-red-contrib-moment
